local assets = {}

assets.tilesets = {}

assets.tilesets.atlas1 = love.graphics.newImage("assets/img/base_out_atlas.png")
assets.tilesets.tanks = love.graphics.newImage("assets/img/topdowntanks.png")
assets.font = love.graphics.newFont("assets/iosevka.ttf", 24)

assets.music = {
	menu = love.audio.newSource("assets/music/menu-theme.ogg", "stream"),
	game = love.audio.newSource("assets/music/tank-theme.ogg", "stream"),
}

assets.sfx = {
	death = love.audio.newSource("assets/sfx/death.wav", "static"),
	machinegun = love.audio.newSource("assets/sfx/machinegun.wav", "static"),
	cannon = love.audio.newSource("assets/sfx/cannon.wav", "static"),
	minelayer = love.audio.newSource("assets/sfx/minelayer.wav", "static")
}

return assets
