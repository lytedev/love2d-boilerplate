# love2d-boilerplate

## Dependencies

* [LÖVE][love] v11.1

## Run

```
git clone https://gitlab.com/lytedev/love2d-boilerplate.git
cd love2d-boilerplate
love .
```

## Can I just download it?

Sure. [Here are the releases!][releases]

## To Do

* Networking code needs to be in its own module
* Currently only works well on LAN due to no client-side prediction
	* http://www.gabrielgambetta.com/client-side-prediction-server-reconciliation.html


[love]: https://love2d.org/
[releases]: https://gitlab.com/lytedev/love2d-boilerplate/tags
