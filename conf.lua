_globals = {
	version = "0.1.7",
	protocolVersion = "0.1.5",

	-- could be not global by passing it via gamestate
	connectionInfo = "127.0.0.1",
	volumes = {
		music = 0.6,
		sfx = 1.0
	}
	-- connectionInfo = "10.10.18.117"
}

function love.conf(t)
	t.window.width = 1280
	t.window.height = 720
	t.window.display = 2
	t.window.fullscreen = false
	t.window.vsync = true
end
