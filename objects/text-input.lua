local utf8 = require("utf8")
local vector = require("lib.vector")

return function(initialText)
	local textinput = {}

	textinput.font = love.graphics.getFont()
	textinput.text = initialText or ""
	textinput.cursor = "|"
	textinput.addCursor = true
	textinput.timer = 0
	textinput.pos = vector(0, 0)
	textinput.width = 300
	textinput.padding = 5
	textinput.maxlength = 512

	function textinput:update(dt)
		self.timer = self.timer + dt
		if self.timer % 1 < 0.5 then
			self.addCursor = true
		else
			self.addCursor = false
		end
	end

	function textinput:draw(dt)
		local h = self.font:getHeight()
		love.graphics.setColor(0.1, 0.1, 0.1, 0.9)
		love.graphics.rectangle("fill", self.pos.x, self.pos.y, self.width + textinput.padding * 2, h + textinput.padding * 2)
		love.graphics.setColor(0.2, 0.2, 0.2, 0.9)
		love.graphics.setLineWidth(2)
		love.graphics.rectangle("line", self.pos.x, self.pos.y, self.width + textinput.padding * 2, h + textinput.padding * 2)
		t = textinput.text
		w, lines = self.font:getWrap(t, self.width)
		t = lines[#lines]
		if self.addCursor then
			t = t .. self.cursor
		end
		love.graphics.printf(t, self.pos.x, self.pos.y, self.width)
	end

	function textinput:textinput(t)
		if t ~= "" then
			self.timer = 0
		end
		if #(textinput.text .. t) < self.maxlength then
			textinput.text = textinput.text .. t
		end
	end

	function textinput:keypressed(key)
		if key == "backspace" then
			self.timer = 0
			local byteoffset = utf8.offset(textinput.text, -1)
			if byteoffset then
				textinput.text = string.sub(textinput.text, 1, byteoffset - 1)
			end
		end
	end

	return textinput
end
