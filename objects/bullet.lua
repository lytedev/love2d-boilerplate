local assets = require("assets")
local vector = require("lib.vector")

local texture = assets.tilesets.tanks

local types = {
	cannon = {
		bsize = vector(20, 35),
		bscale = vector(0.5, 0.5),
		qpos = vector(671, 140)
	},
	machinegun = {
		bsize = vector(20, 35),
		bscale = vector(0.1, 1.0),
		qpos = vector(814, 0)
	},
	minelayer = {
		bsize = vector(48, 47),
		bscale = vector(0.25, 0.25),
		qpos = vector(735, 253),
		color = {1, 0.2, 0.1, 1}
	}
}

return function(type, pos, vel)
	pos = pos or vector(0, 0)
	vel = vel or vector(0, 0)

	local b = {}

	-- print("bullet type", type)

	local t = types[type]
	local qpos = t.qpos
	local bscale = t.bscale
	local bsize = t.bsize

	b.color = t.color or {1, 1, 1, 1}
	b.quad = love.graphics.newQuad(qpos.x, qpos.y, bsize.x, bsize.y, 1024, 512)
	b.pos = pos
	b.vel = vel
	b.scale = bscale

	function b:draw(dt)
		love.graphics.setColor(self.color)
		local rot = b.vel:normalized():angleTo() + (math.pi / 2)
		-- love.graphics.rectangle("fill", self.pos.x, self.pos.y, 32, 32)
		love.graphics.draw(texture, self.quad, self.pos.x, self.pos.y, rot, self.scale.x, self.scale.y, 10, 0)
	end

	return b
end
