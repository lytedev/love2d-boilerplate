local tiny = require("lib.tiny")
local vector = require("lib.vector")

return function()
	local spinner = {}

	spinner.speed = 3
	spinner.angle = 0
	spinner.center = vector(love.graphics.getWidth() / 2, love.graphics.getHeight() / 2)
	spinner.spinRadius = love.graphics.getHeight() / 4
	spinner.radius = 48

	function spinner:currentPosition(dt)
		self.angle = self.angle + (dt * self.speed)
		local offset = vector.fromPolar(self.angle, self.spinRadius)
		return self.center + offset
	end

	function spinner:draw(dt)
		pos = self:currentPosition(dt)
		love.graphics.setColor(0, 0.5, 1, 0.5)
		love.graphics.circle("fill", pos.x, pos.y, self.radius)
	end

	return spinner
end

