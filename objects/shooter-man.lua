tiny = require "lib.tiny"
vector = require "lib.vector"

return function()
	local p = {}

	p.pos = vector(love.graphics.getWidth() / 2, love.graphics.getHeight() / 2)
	p.size = vector(32, 32)
	p.speed = 500

	p.weapons = {
	}

	function p:draw(dt)
		p:move(dt)
		p:render()
	end

	function p:render()
		o = p.pos + (p.size / 2)
		m = vector(love.mouse.getPosition())
		g = o + vector.fromPolar((m - o):angleTo(), p.size.x)
		love.graphics.rectangle("fill", self.pos.x, self.pos.y, self.size.x, self.size.y)
		love.graphics.line(o.x, o.y, g.x, g.y)

		-- love.graphics.setColor(1.0, 1.0, 1.0, 1.0)
		-- love.graphics.setLineWidth(2)
		-- love.graphics.line(m.x, m.y - 16, m.x, m.y + 16)
		-- love.graphics.line(m.x - 16, m.y, m.x + 16, m.y)
	end

	function p:move(dt)
		p.pos = p.pos + p:getMovementVector() * dt
	end

	function p:getMovementVector()
		m = vector(0, 0)
		if love.keyboard.isDown("w") then m.y = m.y - 1 end
		if love.keyboard.isDown("s") then m.y = m.y + 1 end
		if love.keyboard.isDown("a") then m.x = m.x - 1 end
		if love.keyboard.isDown("d") then m.x = m.x + 1 end
		return m:normalized() * self.speed
	end

	return p
end
