local class = require("lib.class")
local assets = require("assets")

local map = class()

DEFAULT_MAP_WIDTH = 8
DEFAULT_MAP_HEIGHT = 8

TILE_SIZE = 32

function map:init(w, h)
	self.canvas = love.graphics.newCanvas() -- TODO: resize?

	self.tileTypes = {
		{
			texture = assets.tilesets.atlas1
		}
	}

	local iw, ih = self.tileTypes[1].texture:getDimensions()
	self.tileTypes[1].quad = love.graphics.newQuad(703, 56, TILE_SIZE, TILE_SIZE, iw, ih)

	self.width = w or DEFAULT_MAP_WIDTH
	self.height = h or DEFAULT_MAP_HEIGHT
	self.tiles = {}
	for x = 1, self.width do
		self.tiles[x] = {}
		for y = 1, self.height do
			self.tiles[x][y] = 1
		end
	end
end

function map:draw()
	love.graphics.setColor(1.0, 1.0, 1.0, 1.0)
	-- love.graphics.setCanvas(self.canvas)
	local tileTypes = self.tileTypes
	local tiles = self.tiles
	for x = 1, self.width do
		for y = 1, self.height do
			-- print(x, y)
			local t = tiles[x][y]
			local tileInfo = tileTypes[t]
			love.graphics.draw(tileInfo.texture, tileInfo.quad, x * (3 + TILE_SIZE), y * (3 + TILE_SIZE))
		end
	end
	-- love.graphics.setCanvas()
	-- love.graphics.draw(self.canvas, 0, 0)
	
	love.graphics.draw(self.tileTypes[1].texture, 300, 0)
end

return map
