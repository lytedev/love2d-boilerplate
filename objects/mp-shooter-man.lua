local tiny = require "lib.tiny"
local vector = require "lib.vector"
local assets = require "assets"

local tank = {
	texture = assets.tilesets.tanks,
	sprite = {
		size = vector(82, 79),
		quad = function() return love.graphics.newQuad(505, 0, 82, 79, 1024, 512) end
	},
	gunSprite = {
		size = vector(21, 57),
		quad = function() return love.graphics.newQuad(841, 368, 21, 57, 1024, 512) end
	}
}

-- TODO: tread movement trail?

return function()
	local p = {}

	-- logical properties
	p.pos = vector(love.graphics.getWidth() / 2, love.graphics.getHeight() / 2)
	p.size = vector(32, 32)
	p.aimpos = vector(0, 0)
	p.respawnTime = 0
	p.color = {1, 1, 1, 1}
	p.name = ""
	p.respawnTime = 0

	p.healthPerc = 1

	p.activeWeaponIndex = 0
	p.activeSecondaryWeaponIndex = 1

	p.activeWeaponCooldownPerc = 1
	p.activeSecondaryWeaponCooldownPerc = 1

	-- properties for drawing tank body
	p.spriteSize = vector(82, 79)
	p.scale = vector(p.size.x / p.spriteSize.x, p.size.y / p.spriteSize.y)
	p.quad = tank.sprite.quad()
	p.texture = tank.texture
	p.angle = 0

	-- properties for drawing tank gun
	p.gQuad = tank.gunSprite.quad()
	p.gSize = tank.gunSprite.size

	function p:draw(dt)
		p:render()
	end

	function p:render()
		if self.respawnTime > 0 then
			love.graphics.setColor(self.color[1], self.color[2], self.color[3], math.max(0, math.min(5, self.respawnTime / 5)))
		else
			love.graphics.setColor(self.color)
		end
		self:renderBody()
		self:renderGun()
		self:renderUnitFrames()

		-- love.graphics.circle("fill", p.pos.x, p.pos.y, p.size.x / 2)

		-- crosshair
		-- love.graphics.setColor(1.0, 1.0, 1.0, 1.0)
		-- love.graphics.setLineWidth(2)
		-- love.graphics.line(m.x, m.y - 16, m.x, m.y + 16)
		-- love.graphics.line(m.x - 16, m.y, m.x + 16, m.y)
	end

	function p:renderUnitFrames(dt)
		pos = vector(math.floor(self.pos.x), math.floor(self.pos.y))
		local barsy = pos.y + self.size.y + (love.graphics.getFont():getHeight() / 2) + 5
		local barwidth = 64
		local barheight = 5
		local barpadding = 2
		local barsx = math.floor(pos.x) - (barwidth / 2)
		love.graphics.setColor(0.1, 0.1, 0.1, 0.25)
		love.graphics.rectangle("fill", barsx - barpadding, barsy - barpadding, barwidth + (2 * barpadding), (barheight * 3) + (barpadding * 4))
		if self.respawnTime <= 0 then
			love.graphics.setColor(0.3, 1.0, 0.1, 1.0)
			love.graphics.rectangle("fill", barsx, barsy, barwidth * self.healthPerc, barheight)
		else
			love.graphics.setColor(1.0, 0.2, 0.1, 1.0)
			love.graphics.rectangle("fill", barsx, barsy, barwidth * self.respawnTime / 5, barheight)
		end
		love.graphics.setColor(0.8, 0.8, 0.2, 1.0)
		love.graphics.rectangle("fill", barsx, barsy + (barheight + barpadding), barwidth * self.activeWeaponCooldownPerc, barheight)
		love.graphics.rectangle("fill", barsx, barsy + ((barheight + barpadding) * 2), barwidth * self.activeSecondaryWeaponCooldownPerc, barheight)
		local w = 300
		love.graphics.setColor(1, 1, 1, 1.0)
		love.graphics.printf(self.name, math.floor(pos.x) - (w / 2), math.floor(pos.y + self.size.y), w, "center", 0, 0.5, 0.5, -(w / 2), 0, 0, 0)
	end

	function p:renderBody(dt)
		local ps2 = (self.spriteSize / 2)
		love.graphics.draw(self.texture, self.quad, math.floor(self.pos.x), math.floor(self.pos.y), self.angle, self.scale.x, self.scale.y, ps2.x, ps2.y, 0, 0)
	end

	function p:renderGun(dt)
		local gps2 = (tank.gunSprite.size / 2)
		gps2.y = gps2.x
		local gs = vector(0.4, 0.4)
		local m = self.aimpos
		love.graphics.draw(self.texture, self.gQuad, math.floor(self.pos.x), math.floor(self.pos.y), (m - self.pos):angleTo() - (math.pi / 2), gs.x, gs.y, gps2.x, gps2.y, 0, 0)
		-- love.graphics.setLineWidth(4)
		-- love.graphics.line(o.x, o.y, g.x, g.y)
	end

	return p
end
