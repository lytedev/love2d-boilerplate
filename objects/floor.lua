local vector = require "lib.vector"
local assets = require "assets"

return function()
	local f = {}

	f.offset = vector(0, 0)
	f.boundaries = vector(10000, 10000)
	f.color = {1, 1, 1, 1}
	f.texture = assets.tilesets.tanks
	f.quad = love.graphics.newQuad(0, 0, 128, 128, 1024, 512)

	function f:draw(dt)
		f:render()
	end

	function f:setOffset(v)
		self.offset = vector(math.floor(v.x / 128), math.floor(v.y / 128))
	end

	function f:setBoundaries(w, h)
		self.boundaries = vector(w, h)
	end

	function f:render()
		love.graphics.setColor(self.color)
		local w, h = love.graphics.getDimensions()
		local ew = (w / 128)
		local eh = (h / 128)
		for x = -ew/2, (ew / 2) + 1, 1 do
			for y = -eh/2, (eh / 2) + 1, 1 do
				love.graphics.draw(self.texture, self.quad, (self.offset.x + x) * 128, (self.offset.y + y) * 128)
			end
		end
		local b = self.boundaries
		local bw = vector(self.boundaries.x / 2, self.boundaries.y / 2)
		love.graphics.setColor(0, 0, 0, 1)
		love.graphics.rectangle("fill", -bw.x - 10000, -bw.y - 10000, 10000, b.y + 10000)
		-- right boundary
		love.graphics.rectangle("fill", bw.x, bw.y - 10000, 10000, bw.y + 10000)
		-- top boundary
		love.graphics.rectangle("fill", -bw.x - 10000, -bw.y - 10000, b.x + 10000, 10000)
		-- bottom boundary
		love.graphics.rectangle("fill", -bw.x - 10000, bw.y, b.x + 10000, 10000)
		-- love.graphics.rectangle("fill", -bw.x, -bw.y, bw.x, bw.y)
	end

	return f
end
