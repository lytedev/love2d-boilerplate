local vector = require "lib.vector"
local assets = require "assets"

local qoffset = vector(735, 253)
local qsize = vector(48, 47)
local texture = assets.tilesets.tanks

-- TODO: random rotation?

return function(pos, size)
	local b = {}
	b.quad = love.graphics.newQuad(qoffset.x, qoffset.y, qsize.x, qsize.y, 1024, 512)
	b.pos = pos or vector(0, 0)
	b.size = size or vector(24, 24)

	b.scale = vector(size.x / qsize.x, size.y / qsize.y)
	b.origin = size / 2

	function b:draw(dt)
		love.graphics.draw(texture, self.quad, self.pos.x - (self.origin.x / 2), self.pos.y - (self.origin.y / 2), 0, self.scale.x, self.scale.y, self.origin.x, self.origin.y, 0, 0)
	end

	return b
end
