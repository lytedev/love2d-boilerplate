return function()
	m = {}

	m.messages = {}

	function m:add(text)
		print("MSGLOG: " .. text)
		table.insert(self.messages, 1, {
			text = text,
			time = 5
		})
	end

	function m:uiDraw(dt)
		self:update(dt)
		self:render()
	end

	function m:render()
		local cline = 0
		local f = love.graphics.getFont()
		local cw = love.graphics.getWidth() / 3
		local h = f:getHeight()
		for i, v in ipairs(self.messages) do
			if i < 10 then
				w, lines = f:getWrap(v.text, cw)
				love.graphics.setColor(1.0, 1.0, 1.0, math.max(v.time / 0.5))
				if cline > 10 then return end
				love.graphics.printf(v.text, 32, 32 + (h * cline), cw)
				cline = cline + #lines
			end
		end
	end

	function m:update(dt)
		local newMessages = {}
		for i, v in ipairs(self.messages) do
			-- print(i, v, v.text, v.time)
			v.time = v.time - dt
			if (v.time > 0) then table.insert(newMessages, v) end
		end
		self.messages = newMessages
	end

	return m
end
