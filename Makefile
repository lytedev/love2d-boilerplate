PROJECT_NAME ?= l2d-test-game
PROJECT_VERSION ?= v0.1.7
PROJECT_BUNDLE ?= ${PROJECT_NAME}_${PROJECT_VERSION}

LOVE2D_DL_VERSION ?= 11.1
LOVE2D_DIR_VERSION ?= 11.1.0

SOURCE_FILES ?= ./assets ./lib ./objects ./scenes ./systems ./ui ./assets.lua ./conf.lua ./main.lua 

BUILD_DIR ?= $(shell pwd)/build
TMP_BUILD_DIR ?= ${BUILD_DIR}
TMP_BUILD_SOURCE ?= ${TMP_BUILD_DIR}/source
ZIP_DEST ?= ${TMP_BUILD_DIR}/${PROJECT_BUNDLE}.love

LOVE_BUILD_FILES_DIR ?= ${BUILD_DIR}/love2d

WIN_64_SUFFIX ?= win64
WIN_64_BUILD_FILES ?= ${LOVE_BUILD_FILES_DIR}/${WIN_64_SUFFIX}
WIN_64_LOVE_ZIP_URL ?= https://bitbucket.org/rude/love/downloads/love-${LOVE2D_DL_VERSION}-win64.zip
WIN_64_LOVE_FILES ?= ${LOVE_BUILD_FILES_DIR}/${WIN_64_SUFFIX}/love-${LOVE2D_DIR_VERSION}-win64
WIN_64_LOVE_EXE ?= ${WIN_64_LOVE_FILES}/love.exe
WIN_64_EXE ?= ${TMP_BUILD_DIR}/${PROJECT_NAME}_${PROJECT_VERSION}_${WIN_64_SUFFIX}.exe
WIN_64_RELEASE_DIR ?= ${TMP_BUILD_DIR}/${PROJECT_NAME}_${PROJECT_VERSION}_${WIN_64_SUFFIX}-release-files
WIN_64_RELEASE ?= ${TMP_BUILD_DIR}/${PROJECT_NAME}_${PROJECT_VERSION}_${WIN_64_SUFFIX}-release.zip
# WIN_64_LOVE_DIR ?=

LINUX_64_SUFFIX ?= linux64
MACOS_64_SUFFIX ?= macos64

LINUX_64_BUILD_FILES ?= ${LOVE_BUILD_FILES_DIR}/${LINUX_64_SUFFIX}
MACOS_64_BUILD_FILES ?= ${LOVE_BUILD_FILES_DIR}/${MACOS_64_SUFFIX}


default: run

run:
	love .

clean: "clean_${ZIP_DEST}"
clean-all: clean clean-build-dir clean-love_build_files

clean-build-dir:
	rm -rf "${BUILD_DIR}"

clean-love_build_files:
	rm -rf "${LOVE_BUILD_FILES_DIR}"

"clean_${ZIP_DEST}":
	rm -f "${ZIP_DEST}"

build: love_build_files ${ZIP_DEST}

${ZIP_DEST}:
	mkdir -p "${TMP_BUILD_SOURCE}"
	cp -r ${SOURCE_FILES} "${TMP_BUILD_SOURCE}"
	cd "${TMP_BUILD_SOURCE}" && zip -9 -r "${ZIP_DEST}" .

love_build_files: win_64_build_files# linux_64_build_files macos_64_build_files


# WINDOWS
win_64_build_files: build_win_64_artifacts

build_win_64_artifacts: ${WIN_64_EXE} ${WIN_64_RELEASE}

${WIN_64_EXE}: ${WIN_64_LOVE_EXE} ${ZIP_DEST}
	cat "${WIN_64_LOVE_EXE}" "${ZIP_DEST}" > "${WIN_64_EXE}"

${WIN_64_LOVE_EXE}:
	mkdir -p "${WIN_64_BUILD_FILES}"
	cd "${WIN_64_BUILD_FILES}" && yes | curl -L -O "${WIN_64_LOVE_ZIP_URL}"
	cd "${WIN_64_BUILD_FILES}" && yes | unzip "./love-${LOVE2D_DL_VERSION}-win64.zip"

${WIN_64_RELEASE}: ${WIN_64_EXE} ${WIN_64_LOVE_FILES}/OpenAL32.dll ${WIN_64_LOVE_FILES}/SDL2.dll ${WIN_64_LOVE_FILES}/love.dll ${WIN_64_LOVE_FILES}/lua51.dll ${WIN_64_LOVE_FILES}/mpg123.dll ${WIN_64_LOVE_FILES}/msvcp120.dll ${WIN_64_LOVE_FILES}/msvcr120.dll
	mkdir -p "${WIN_64_RELEASE_DIR}"
	cp ${WIN_64_EXE} ${WIN_64_LOVE_FILES}/OpenAL32.dll ${WIN_64_LOVE_FILES}/SDL2.dll ${WIN_64_LOVE_FILES}/love.dll ${WIN_64_LOVE_FILES}/lua51.dll ${WIN_64_LOVE_FILES}/mpg123.dll ${WIN_64_LOVE_FILES}/msvcp120.dll ${WIN_64_LOVE_FILES}/msvcr120.dll "${WIN_64_RELEASE_DIR}"
	cd ${WIN_64_RELEASE_DIR} && zip -9 -r "${WIN_64_RELEASE}" .
	# TODO: modify the exe icon

# LINUX
linux_64_build_files:
	@echo "Linux Build Coming... Someday..."

# MACOS
macos_64_build_files:
	@echo "macOS Build Coming... Someday..."
