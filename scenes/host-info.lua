local gamestate = require("lib.gamestate")

local utf8 = require("utf8")

local menu = {}

function menu:enter()
	menu.text = _globals.connectionInfo
	menu.cursor = "|"
	menu.addCursor = true
	menu.timer = 0
	self.initialKeyRepeat = love.keyboard.hasKeyRepeat()
	love.keyboard.setKeyRepeat(true)
end

function menu:leave()
	love.keyboard.setKeyRepeat(self.initialKeyRepeat)
end

function menu:update(dt)
	self.timer = self.timer + dt
	if self.timer % 1 < 0.5 then
		self.addCursor = true
	else
		self.addCursor = false
	end
end

function menu:draw()
	t = menu.text
	if self.addCursor then
		t = t .. self.cursor
	end
	w, lines = love.graphics.getFont():getWrap(t, love.graphics.getWidth() - 64)
	lines = #lines
	text = [[Press Escape to Cancel
Press Enter to Confirm

]] .. t .. [[


Format: $HOST:$PORT (default port is 6090)
	Example 1: 127.0.0.1
	Example 2: lytedev.io
	Example 3: lytedev.io:6095
	Example 4: 10.10.100.100:9001
	
Due to crappy netcode, stick to LAN]]
	love.graphics.printf(text, 32, 32, love.graphics.getWidth() - 64)
end

function menu:textinput(t)
	if t ~= "" then
		self.timer = 0
	end
	if #(menu.text .. t) < 512 then
		menu.text = menu.text .. t
	end
end

function menu:keypressed(key)
	if key == "return" then
		_globals.connectionInfo = menu.text
		gamestate.pop()
	end
	if key == "escape" then
		gamestate.pop()
	end
	if key == "backspace" then
		self.timer = 0
		local byteoffset = utf8.offset(menu.text, -1)
		if byteoffset then
			menu.text = string.sub(menu.text, 1, byteoffset - 1)
		end
	end
end

return menu
