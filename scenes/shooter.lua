local scene = {}

function scene:enter()
	self.world = tiny.world(
		require("systems.drawable")(),
		require("objects.shooter-man")()
	)
end

function scene:leave()
	tiny.clearEntities(self.world)
	tiny.clearSystems(self.world)
end

function scene:draw(dt)
	if self.world then
		self.world:update(love.timer.getDelta())
	end
end

function scene:keypressed(key)
	if key == "escape" then
		gamestate.push(require("scenes.main-menu"))
	end
end

return scene
