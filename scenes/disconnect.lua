local socket = require "socket"
local utf8 = require "utf8"

local scene = {}

scene.color = {1.0, 1.0, 1.0, 1.0}

function scene:enter(from, disconnectMessage)
	print("disconnect:enter()", self, disconnectMessage, a)
	self.disconnectMessage = disconnectMessage

	if self.disconnectMessage == "client_not_found" then
		self.disconnectMessage = "CLIENT_NOT_FOUND (Handshake failed or server restarted/crashed)"
	end
end

function scene:draw()
	love.graphics.setColor(self.color)
	msg = self.disconnectMessage .. "\n\nPress R to try again\nPress Escape to return the the Main Menu"
	love.graphics.printf(msg, 32, 32, love.graphics.getWidth() - 64)
end

function scene:keypressed(key)
	if key == "r" then
		gamestate.switch(require("scenes.multiplayer"))
	end
	if key == "escape" then
		gamestate.switch(require("scenes.main-menu"))
	end
end

return scene
