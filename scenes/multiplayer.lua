local tiny = require("lib.tiny")
local vector = require("lib.vector")
local gamestate = require("lib.gamestate")
local camera = require("lib.camera")
local socket = require("socket")
local assets = require("assets")

local utf8 = require "utf8"

local game = {}

-- TODO: use a binary networking protocol?

function game:enter()
	assets.music.game:setVolume(_globals.volumes.music)
	assets.music.game:play()

	self.messages = require("objects.game-messages-window")()

	self.netUpdateTime = 1 / 60
	self.lastNetUpdate = 0
	self.isConnected = false
	self.connectionId = ""

	self.camera = camera.new()

	self.myID = nil
	self.me = nil
	self.players = {}
	self.bullets = {}
	self.floor = require("objects.floor")()

	self.floor.color = {0, 0, 0, 0}

	self.barrels = {}

	self:connect()
	self:initServerDatagramHandlers()

	self.world = tiny.world(
		self.messages,
		require("systems.drawable")(self.camera),
		require("systems.ui_drawable")(),
		self.floor,
		require("ui.fps-counter")()
	)
end

function game:leave()
	assets.music.game:stop()

	tiny.clearEntities(self.world)
	tiny.clearSystems(self.world)
	self:addMessage("Quitting Multiplayer...")
	self:quit()
end

function game:getConnectionInfo()
	local ci = _globals.connectionInfo
	-- hopefully supports ipv6?
	if ci:find(":%d+$") == nil then
		return ci, 6090
	else
		return ci:match("(.*):(%d+)$")
	end
end

function game:connect()
	self:addMessage("Preparing Multiplayer...")
	self:addMessage("Creating Socket...")
	self.udp = socket.udp()
	self.udp:settimeout(0)
	local ip, port = self:getConnectionInfo()
	self:addMessage("Attempting connection to " .. ip .. ":" .. port .. "...")
	self.udp:setpeername(ip, port)
	self:connectHandshake()
end

function game:connectHandshake()
	-- TODO: some unique id?
	local dir = love.filesystem.getUserDirectory()
	-- print(dir)
	local name =
		dir:match("/home/([^/]+)")
		or dir:match("\\Documents and Settings\\([^\\\\]+)")
		or dir:match("\\Users\\([^\\\\]+)")
		or "Unnamed Player"
	self.udp:send("l2d_test_game v" .. _globals.protocolVersion .. " connect " .. name)
end

function game:addMessage(...)
	self.messages:add(...)
end

function game:handleSendNetUpdate(dt)
	self.lastNetUpdate = self.lastNetUpdate + dt
	if self.lastNetUpdate > self.netUpdateTime then
		self.lastNetUpdate = self.lastNetUpdate % self.netUpdateTime
		self:sendNetUpdate(dt)
	end
end

function game:sendNetUpdate()
	if not self.isConnected then return end
	local mx, my = self.camera:worldCoords(love.mouse.getPosition())
	local up = love.keyboard.isDown("w") and 1 or 0
	local down = love.keyboard.isDown("s") and 1 or 0
	local left = love.keyboard.isDown("a") and 1 or 0
	local right = love.keyboard.isDown("d") and 1 or 0
	local fire = (love.keyboard.isDown("space") or love.mouse.isDown(1)) and 1 or 0
	local activeWeaponIndex = self.me.activeWeaponIndex
	local activeSecondaryWeaponIndex = self.me.activeSecondaryWeaponIndex
	local secondaryFire = (love.keyboard.isDown("lshift") or love.mouse.isDown(2)) and 1 or 0
	local inputStates = up .. "," .. down .. "," .. left .. "," .. right .. "," .. fire .. "," .. secondaryFire
	self.udp:send("up " .. inputStates .. " " .. mx .. "," .. my .. " " .. activeWeaponIndex .. " " .. activeSecondaryWeaponIndex)
end

function game:handleReceiveNetUpdate(dt)
	hasPackets = true
	while hasPackets do
		local data, msg = self.udp:receive(100000000)
		if data then
			print("UDPDatagram:", tostring(data))
			-- self:addMessage(tostring(data))
			if data:match("^disconnected ") then
				-- print("disconnected", data, data:sub(14))
				gamestate.switch(require("scenes.disconnect"), data:sub(14))
			elseif not self.isConnected then
				print("CONNECTED", data)
				local isConnected = data:match("^l2d_test_game v" .. _globals.protocolVersion .. " accepted ")
				self.floor.color = {1, 1, 1, 1}
				-- TODO: store connectionId
				if isConnected then
					self.myID = data:match("[^ ]+ [^ ]+ [^ ]+ ([%w%-%_]+) ")
					local w, h = data:match(" ([^ ]+) ([^ ]+)$")
					print(w, h)
					self.floor:setBoundaries(w, h)
					self.isConnected = true
					self:handleConnected(dt)
				end
			else
				self:handleServerDatagram(data)
			end
		else
			-- print("UDP TIMEOUT?", data, msg)
			hasPackets = false
		end
	end
end

function game:handleConnected()
	self:addMessage("Connected to " .. _globals.connectionInfo)
end

local numberMatcher = "([%%d%%.%%-e/]+)"
local clientUpdatePattern = ("&,& &,&,&,& &,& &,& & & & & &"):gsub("&", numberMatcher) .. " ([%w%-_]+)"
local bulletUpdatePattern = ("&,& &,& &"):gsub("&", numberMatcher)
local barrelPacketPattern = ("&,& &,&,&,& &,& & &"):gsub("&", numberMatcher)

function game:handleServerUpdateString(s)
	local game = self
	local j1, j2 = s:find("%d-\n")
	local num_players = tonumber(s:sub(j1, j2))
	if num_players == nil then
		print("Possible bad server update string: " .. tostring(s))
		return
	end
	local rem = s:gsub("\n" .. clientUpdatePattern, function(w, h, r, g, b, a, x, y, ax, ay, awc, aswc, hp, score, respawnTime, id)
		local pl = game.players[id]
		if pl == nil then
			print("Received data for unknown player with id " .. id .. "\n\t" .. s)
			return
		else
		end
		pl.size.x = tonumber(w)
		pl.size.y = tonumber(h)
		pl.color = {tonumber(r), tonumber(g), tonumber(b), tonumber(a)}
		local curPos = vector(pl.pos.x, pl.pos.y)
		pl.pos.x = tonumber(x)
		pl.pos.y = tonumber(y)
		if pl.pos.x ~= curPos.x or pl.pos.y ~= curPos.y then
			pl.angle = (pl.pos - curPos):angleTo() + (math.pi / 2)
			-- print(curPos, pl.pos, pl.angle)
		end
		pl.aimpos.x = tonumber(ax)
		pl.aimpos.y = tonumber(ay)
		pl.score = tonumber(score)
		pl.respawnTime = tonumber(respawnTime)
		pl.activeWeaponCooldownPerc = awc
		pl.activeSecondaryWeaponCooldownPerc = aswc
		-- pl.name = name
		oldHealth = pl.healthPerc
		pl.healthPerc = tonumber(hp)
		if oldHealth > 0 and tonumber(hp) <= 0 then
			self:playRelativePositionSound(assets.sfx.death, pl.pos.x, pl.pos.y)
		end
		if pl == self.me then
			self.floor:setOffset(pl.pos)
		end
		return ""
	end, num_players)
	rem = rem:sub(2)
	j1, j2 = rem:find("\n?%d-[ \n]")
	local num_bullets = tonumber(rem:sub(j1, j2))
	-- print("After Parse Client Pieces from Packet", "\"" .. rem .. "\"")
	if num_bullets == nil then
		print("Possible bad server update string (bullets portion): " .. tostring(s))
		return
	end
	local rem = rem:gsub("\n" .. bulletUpdatePattern, function(x, y, vx, vy, id)
		-- print("parse bullet update", rem, num_bullets)
		-- print(rem:match(bulletUpdatePattern), bulletUpdatePattern)
		-- print("BULLET", "\"" .. id .. "\"")
		-- print(tonumber(id))
		local b = game.bullets[tonumber(id)]
		if b ~= nil then
			b.pos.x = tonumber(x)
			b.pos.y = tonumber(y)
			b.vel.x = tonumber(vx)
			b.vel.y = tonumber(vy)
		end
		return ""
	end, num_bullets)
end

function game:update(dt)
	self:handleSendNetUpdate(dt)
	-- hack - handle multiple packets at once
	self:handleReceiveNetUpdate(dt)
	if self.world then
		self.world:update(love.timer.getDelta())
	end
end

function game:draw()
	if self.me ~= nil then
		self.camera:lookAt(math.floor(self.me.pos.x), math.floor(self.me.pos.y))
		-- self.camera:zoomTo(0.5)
	end

	if self.world then
		self.world:update(love.timer.getDelta())
	end

	if love.keyboard.isDown("tab") then
		local w, h = love.graphics.getDimensions()
		local x = w / 4
		local y = h / 4
		w = w / 2
		h = h / 2
		love.graphics.setColor(0, 0, 0, 0.5)
		love.graphics.rectangle("fill", x, y, w, h)
		love.graphics.setColor(1, 1, 1, 1.0)
		love.graphics.printf("Leaderboard", x + 5, y + 5, w - 10, "center")
		love.graphics.setColor(1, 1, 1, 0.8)
		local sortedPlayers = self.players
		table.sort(sortedPlayers, function(a, b) return a.score < b.score end)
		lines = 1
		for k, p in pairs(sortedPlayers) do
			local oy = lines * love.graphics.getFont():getHeight()
			love.graphics.printf(p.name, x + 15, y + 15 + oy, w - 30, "left")
			love.graphics.printf(tostring(p.score), x + 15, y + 15 + oy, w - 30, "right")
			lines = lines + 1
		end
	end

	self:drawMessages()
	-- love.graphics.print(tostring(#self.barrels), 500, 500)
end

function game:drawMessages()
end

function game:textinput(t)
end

function game:keypressed(key)
	if key == "escape" then
		gamestate.switch(require("scenes.main-menu"))
	end
	if key == "f" then
		if self.me ~= nil then
			if self.me.activeSecondaryWeaponIndex == 1 then
				self.me.activeSecondaryWeaponIndex = 2
			else
				self.me.activeSecondaryWeaponIndex = 1
			end
		end
	end
end

function game:initServerDatagramHandlers()
	self.sdgHandlers = {
		new_barrel = function(game, argstr)
			--local barrelPacketPattern = ("&,& &,&,&,& &,& & &"):gsub("&", numberMatcher)
			argstr:gsub(barrelPacketPattern, function (x, y, r, g, b, a, w, h, hp, id)
				-- print("new barrel:", x, y, r, g, b, a, w, h, hp, id)
				local size = vector(tonumber(w), tonumber(h))
				local b = require("objects.barrel")(vector(tonumber(x), tonumber(y)), size)
				b.color = {r,g,b,a}
				game.barrels[tonumber(id)] = b
				game.world:addEntity(b)
			end)
		end,
		remove_barrel = function(game, argstr)
			-- print("remove barrel", argstr)
			game.world:removeEntity(game.bullets[tonumber(argstr)])
			game.bullets[argstr] = nil
		end,
		new_bullet = function(game, argstr)
			argstr:gsub("([%w%-_]-) ([%w%-_]-) " .. bulletUpdatePattern, function (owner, type, x, y, vx, vy, id)
				-- print("new bullet:", type, x, y, vx, vy, id)
				local x = tonumber(x)
				local y = tonumber(y)
				local b = require("objects.bullet")(type, vector(x, y), vector(tonumber(vx), tonumber(vy)))
				if type == "minelayer" then
					self:playRelativePositionSound(assets.sfx.minelayer, x, y)
				end
				if type == "cannon" then
					self:playRelativePositionSound(assets.sfx.cannon, x, y)
				end
				if type == "machinegun" then
					self:playRelativePositionSound(assets.sfx.machinegun, x, y)
				end
				game.bullets[tonumber(id)] = b
				game.world:addEntity(b)
			end)
		end,
		remove_bullet = function(game, argstr)
			-- print("remove bullet", argstr)
			game.world:removeEntity(game.bullets[tonumber(argstr)])
			game.bullets[argstr] = nil
		end,
		new_client = function(game, argstr)
			-- print(argstr)
			local defaults = {
				size = vector(0, 0),
				pos = vector(0, 0),
				aimpos = vector(0, 0),
				color = {0, 0, 0, 0},
				score = 0,
			}
			local p = require("objects.mp-shooter-man")()
			for k,v in pairs(defaults) do p[k] = v end
			argstr:gsub("([%w%-_]+) ([%w%-_/%.]+)", function(id, name)
				-- print(id, name)
				if id == self.myID then
					self.me = p
				end
				game.players[id] = p
				p.name = name
				game.world:addEntity(p)
				game:addMessage(p.name .. " connected.")
			end)
		end,
		remove_client = function(game, argstr)
			-- print(argstr)
			p = game.players[argstr]
			if p then
				game:addMessage(p.name .. " disconnected.")
				game.world:removeEntity(p)
				game.players[argstr] = nil
			end
		end,
		msg = function(game, argstr)
			-- print("RECEIVED MSG", argstr)
			self:addMessage(argstr)
		end,
		up = self.handleServerUpdateString
	}
end

function game:playRelativePositionSound(sound, x, y)
	x = x - self.me.pos.x
	y = y - self.me.pos.y
	sound:setPosition(x / 100, y / 100)
	sound:setPitch(math.random() * 0.2 + 0.9)
	sound:setVolume(_globals.volumes.sfx)
	sound:play()
end

function game:handleServerDatagram(data)
	s, e, command = data:find("^([^ \n]+)")
	local f = self.sdgHandlers[command]
	if f then
		f(self, data:sub(#(command) + 2))
	end
end

function game:quit()
	self.udp:send("quit")
end

return game
