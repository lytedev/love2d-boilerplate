local tiny = require("lib.tiny")
local gamestate = require("lib.gamestate")
local vector = require("lib.vector")

local game = {}

local speedFactor = 1

function game:enter()
	spinner1 = require("objects.spinner")()
	spinner1.speed = 1

	spinner2 = require("objects.spinner")()
	spinner2.speed = 6
	spinner2.radius = 12
	spinner2.spinRadius = 100

	spinner3 = require("objects.spinner")()
	spinner3.speed = 10
	spinner3.radius = 12
	spinner3.spinRadius = 100
	spinner3._draw = spinner3.draw
	function spinner3:draw(dt)
		self.center = spinner1:currentPosition(dt)
		spinner3:_draw(dt)
	end

	self.world = tiny.world(
		require("systems.ui_drawable")(),
		require("systems.drawable")(),
		require("objects.map")(),
		spinner1,
		spinner2,
		spinner3,
		require("ui.fps-counter")()
	)
end

function game:leave()
	tiny.clearEntities(self.world)
	tiny.clearSystems(self.world)
end

function game:draw()
	if self.world then
		self.world:update(love.timer.getDelta() * speedFactor)
	end

	love.graphics.print("Speed Factor: " .. speedFactor, 32, 350)
end

function game:keypressed(key)
	if key == "up" then
		speedFactor = speedFactor + 0.1
	end
	if key == "down" then
		speedFactor = speedFactor - 0.1
	end
	if key == "escape" then
		gamestate.pop()
	end
end

return game
