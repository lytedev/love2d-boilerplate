local menu = {}

local gamestate = require "lib.gamestate"
local assets = require "assets"

function menu:enter()
	assets.music.menu:setVolume(_globals.volumes.music)
	assets.music.menu:play()

	love.keyboard.setKeyRepeat(true)
end

function menu:leave()
	assets.music.menu:stop()
	love.keyboard.setKeyRepeat(false)
end

function menu:menuText()
	return "Main Menu\n\n" .. 
		"Press H to set Host Connection Info\nPress C to Join (" .. _globals.connectionInfo .. ")\n\n" ..
		"Press P for Test Scene\n\n" ..
		"Music Volume (Down/Up Arrow Keys)   : " .. math.floor(_globals.volumes.music * 100) .. "%\n" ..
		"SFX Volume   (Left/Right Arrow Keys): " .. math.floor(_globals.volumes.sfx * 100) .. "%\n\n" ..
		"Press Escape to Quit"
end

function menu:draw()
	love.graphics.setFont(assets.font)
	love.graphics.setColor(1.0, 1.0, 1.0, 1.0)
	love.graphics.print(self:menuText(), 32, 32)
end

function menu:keypressed(key)
	if key == "escape" then
		love.event.quit()
	end
	if key == "p" then
		gamestate.push(require("scenes.game"))
	end
	if key == "z" then
		-- gamestate.switch(require("scenes.shooter"))
	end
	if key == "h" then
		gamestate.push(require("scenes.host-info"))
	end
	if key == "c" then
		gamestate.switch(require("scenes.multiplayer"))
	end

	if key == "up" then
		_globals.volumes.music = math.min(_globals.volumes.music + 0.01, 1)
		assets.music.menu:setVolume(_globals.volumes.music)
	end

	if key == "down" then
		_globals.volumes.music = math.max(_globals.volumes.music - 0.01, 0)
		assets.music.menu:setVolume(_globals.volumes.music)
	end

	if key == "right" then
		_globals.volumes.sfx = math.min(_globals.volumes.sfx + 0.01, 1)
	end

	if key == "left" then
		_globals.volumes.sfx = math.max(_globals.volumes.sfx - 0.01, 0)
	end
end

return menu
