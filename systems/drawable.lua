local tiny = require("lib.tiny")
local Camera = require("lib.camera")

return function(camera)
	local DrawableSystem = tiny.processingSystem()

	DrawableSystem.filter = tiny.requireAny("draw")
	DrawableSystem.camera = camera or Camera.new()

	function DrawableSystem:preProcess(dt)
		self.camera:attach()
	end

	function DrawableSystem:process(e, dt)
		e:draw(dt)
	end

	function DrawableSystem:postProcess(dt)
		self.camera:detach()
	end

	return DrawableSystem
end
