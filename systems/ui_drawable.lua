local tiny = require("lib.tiny")

return function(camera)
	local UIDrawableSystem = tiny.processingSystem()

	UIDrawableSystem.filter = tiny.requireAny("uiDraw")

	function UIDrawableSystem:preProcess(dt)
	end

	function UIDrawableSystem:process(e, dt)
		e:uiDraw(dt)
	end

	function UIDrawableSystem:postProcess(dt)
	end

	return UIDrawableSystem
end
