return function()
	local fpsCounter = {}

	function fpsCounter:uiDraw()
		love.graphics.setColor(1, 1, 1, 0.5)
		local h = love.graphics.getHeight() - 20 - love.graphics.getFont():getHeight()
		love.graphics.print(tostring(love.timer.getFPS()) .. " FPS", 20, h)
	end

	return fpsCounter
end
