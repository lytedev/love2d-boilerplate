local gamestate = require "lib.gamestate"

function love.load()
	loadSettings()

	local assets = require "assets"
	local mainMenu = require "scenes.main-menu"

	local f = love.graphics.newFont(24)
	love.graphics.setFont(f)
	gamestate.registerEvents()
	gamestate.switch(mainMenu)

	-- love.mouse.setVisible(false)
end

function love.quit()
	serializeSettings()
end

function serializeSettings()
	local f = love.filesystem.newFile("settings.lua")
	f:open("w")
	f:write("_globals.volumes={music=" .. tostring(_globals.volumes.music) .. ",sfx=" .. tostring(_globals.volumes.sfx) .. "}")
	f:close()
end

function loadSettings()
	local f = love.filesystem.newFile("settings.lua")
	f:open("r")
	local data = f:read()
	f:close()
	local ok, f, e = pcall(loadstring, data)
	if not ok then
		print("Failed to load settings.")
	else
		ok, result = pcall(f)
		print(ok, f, e, result)
	end
end
